#  Task details which I have done

- I have created the APIClient class to fetch the data from server for all apis we can use the same function which we have in this class.
- Used model classes for every api response so that we can easily access/assign the variable and it will give us the code understating/clean for the other people
- I have used the pods to integrated the Kingfisher 3rd party library for image cacheing purpose and it will make smooth scrolling.
- Implemented the rating view as per the UI for this I have used CASharpLayer which is from xcode it self that is quartz core framework
- I have not done the UI test cases because I have limited knowledge on that.
- Implemented the details screen as per the requirement.
- The app will supports ios version 13.x
- Th app will supports portrait mode only.
