//
//  APIClient.swift
//  CS_iOS_Assignment
//
//  Created by Hyper Thread Solutions on 14/12/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

func header()->[String:String]{
    return [
        "Content-Type":"application/json",
    ]
}
class URLSessionManager{

    private init(){}
    
    typealias failure = (String)-> Void

     enum HTTPMethod: String {
        case get     = "GET"
        case post    = "POST"
        case put     = "PUT"
    }
    

    static func hitApi<T: Codable>(urlString: String, parameters:[String:Any] = [:],method:HTTPMethod = .get,header:[String:String] = header() ,success:@escaping(T)-> (),failure: @escaping failure){
        if !Reachability.isConnectedToNetwork(){
            failure("Internet Connection not Available!")
        }
        print("parameters",parameters)
        guard let url = URL(string: urlString) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.allHTTPHeaderFields = header
        if method == .post {
            request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)
        }
        URLSession.shared.dataTask(with: request) { (data,response ,error ) in
            if error != nil{
                failure(error?.localizedDescription ?? "")
                return
            }
            guard let data = data else {
                failure("Data Not Found")
                return
            }
            if let resp = response as? HTTPURLResponse{
                print("status Code \(resp.statusCode)")
            }
            if let resp = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves){
                print("resp = ",resp)
            }
            guard let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 else {
                print(error as Any)
                failure(error?.localizedDescription ?? "")
                return
            }
            do{
                let respData = try JSONDecoder().decode(T.self, from: data)
                success(respData)
            }catch let jsonErr {
                print(jsonErr)
                failure(jsonErr.localizedDescription)
                return
            }
        }.resume()
    }
}
