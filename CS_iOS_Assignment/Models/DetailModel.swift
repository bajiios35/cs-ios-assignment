//
//  DetailModel.swift
//  CS_iOS_Assignment
//
//  Created by Hyper Thread Solutions on 15/12/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import Foundation

struct DetailModel: Codable {
    let posterPath: String?
    let title: String?
    let originalTitle: String?
    let overview: String?
    let releaseDate: String?
    let genres: [Genre]?
    let runtime: Int?
    

    enum CodingKeys: String, CodingKey {
        case genres
        case originalTitle = "original_title"
        case overview
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case runtime
        case title
    }
}

// MARK: - Genre
struct Genre: Codable {
    let id: Int?
    let name: String?
}
