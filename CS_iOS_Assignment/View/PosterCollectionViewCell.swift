//
//  PosterCollectionViewCell.swift
//  CS_iOS_Assignment
//
//  Created by Hyper Thread Solutions on 14/12/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit
import Kingfisher

class PosterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img_Poster: UIImageView!
        
    func configure(obj_Poster: PosterResult) {
        let strUrl = "http://image.tmdb.org/t/p/w185/" + (obj_Poster.posterPath ?? "")
        let url = URL(string: strUrl)
        img_Poster.kf.setImage(with: url)
    }
}
