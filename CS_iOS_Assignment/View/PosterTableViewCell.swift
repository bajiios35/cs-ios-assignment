//
//  PosterTableViewCell.swift
//  CS_iOS_Assignment
//
//  Created by Hyper Thread Solutions on 14/12/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

class PosterTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView_Poster: UICollectionView!
    var array_Poster = [PosterResult]()
    var clouser:((PosterResult)->())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.collectionView_Poster.dataSource = self
        self.collectionView_Poster.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(arra_poster: [PosterResult])
    {
        self.array_Poster = arra_poster
        self.collectionView_Poster.reloadData()
    }

}

extension PosterTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array_Poster.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PosterCollectionViewCell = self.collectionView_Poster.dequeueReusableCell(withReuseIdentifier: "posterCell", for: indexPath) as! PosterCollectionViewCell
        
        cell.configure(obj_Poster: array_Poster[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        clouser?(array_Poster[indexPath.item])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 160.0)
    }
    
}
