//
//  MovieCell.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit
import Kingfisher
//
// MARK: - Movie Cell
//
class MovieCell: UITableViewCell {
    
    //
    // MARK: - Class Constants
    //
    static let identifier = "MovieCell"
    
    //
    // MARK: - IBOutlets
    @IBOutlet weak var lbl_rating_Percentage: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rating: RatingView!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var poster: UIImageView!
    
    func configure(obj_Popular: PopularResult) {
        title.text = obj_Popular.title
        releaseDate.text = obj_Popular.releaseDate
        let strUrl = "http://image.tmdb.org/t/p/w185/" + (obj_Popular.posterPath ?? "")
        let url = URL(string: strUrl)
        poster.kf.setImage(with: url)
        rating.initSubviews(value: obj_Popular.voteAverage ?? 0.0)
        lbl_rating_Percentage.text =  String(format: "%d", Int((obj_Popular.voteAverage ?? 0.0)*10.0)) + "%"
    }
}
