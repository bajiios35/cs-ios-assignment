//
//  DetailViewController.swift
//  CS_iOS_Assignment
//
//  Created by Hyper Thread Solutions on 14/12/20.
//  Copyright © 2020 Backbase. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var collectionView_Generes: UICollectionView!
    @IBOutlet weak var lbl_Description: UILabel!
    @IBOutlet weak var lbl_ReleaseDate: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_Poster: UIImageView!
    
    private var obj_Detail: DetailModel?
    
    var movieID = Int()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        img_Poster?.layer.cornerRadius = 2.0
        img_Poster?.layer.borderWidth = 0.5
        img_Poster?.layer.borderColor = UIColor.white.cgColor
        
        DetailAPi()
        
    }
    
    func DetailAPi()
    {
        URLSessionManager.hitApi(urlString: "https://api.themoviedb.org/3/movie/\(movieID)?api_key=55957fcf3ba81b137f8fc01ac5a31fb5", method: URLSessionManager.HTTPMethod.get) { (response: DetailModel) in
            self.obj_Detail = response
            DispatchQueue.main.async {
                if let obj = self.obj_Detail
                {
                    self.configure(obj_Detail: obj)
                    self.collectionView_Generes.reloadData()
                }
            }
        } failure: { (error) in
            print(error)
        }
    }
    
    func configure(obj_Detail: DetailModel) {
        self.lbl_Title.text = obj_Detail.title
        let time = timeFormatted((obj_Detail.runtime ?? 0) * 60)
        self.lbl_ReleaseDate.text = (obj_Detail.releaseDate ?? "") + "  -  " + time
        self.lbl_Description.text = obj_Detail.overview
        let strUrl = "http://image.tmdb.org/t/p/w185/" + (obj_Detail.posterPath ?? "")
        let url = URL(string: strUrl)
        self.img_Poster.kf.setImage(with: url)
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = (totalSeconds / 3600) % 60
        return String(format: "%02dh %02dm",hours, minutes)
    }
    
    @IBAction func btn_CancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return obj_Detail?.genres?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView_Generes.dequeueReusableCell(withReuseIdentifier: "genersCell", for: indexPath)
        let lbl_Genere = cell.contentView.viewWithTag(1) as? UILabel
        lbl_Genere?.text = obj_Detail?.genres?[indexPath.item].name
        lbl_Genere?.backgroundColor = .white
        lbl_Genere?.textColor = .black
        lbl_Genere?.layer.cornerRadius = 2.0
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.text = self.obj_Detail?.genres?[indexPath.item].name ?? ""
        label.sizeToFit()
        return CGSize(width: label.frame.width + 30, height: 25)
    }
}
