//
//  ViewController.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    var jsonArray: [Any] = []
    var isPaginating: Bool = true
    var pageNo = 1
    let movieService = MovieService()
    
    var array_Popular = [PopularResult]()
    var array_Poster = [PosterResult]()
    
    lazy var createSpinner:UIView = {
    let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.moviesTableView.frame.size.width, height: 120))
    let spinner = UIActivityIndicatorView()
    footerView.backgroundColor = .lightText
    spinner.center = footerView.center
    footerView.addSubview(spinner)
    spinner.startAnimating()
    return footerView
    }()
    
    
    @IBOutlet weak var moviesTableView: UITableView!
//    let concurrentQueue = DispatchQueue(label: "com.queue.Concurrent", attributes: .concurrent)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.moviesTableView.delegate = self
        self.moviesTableView.dataSource = self
        performAsyncTaskIntoConcurrentQueue(with: {
            self.moviesTableView.reloadData()
        })

        
//        movieService.fetchMovies { jsonArray in
//            if let jsonArray = jsonArray {
//                self.jsonArray = jsonArray
//                self.moviesTableView.reloadData()
//            }
//        }
    }
    
    
    func performAsyncTaskIntoConcurrentQueue(with completion: @escaping () -> ()) {
            let group = DispatchGroup()
                group.enter()
//                concurrentQueue.async {
                    URLSessionManager.hitApi(urlString: "https://api.themoviedb.org/3/movie/now_playing?language=en-US&page=undefined&api_key=55957fcf3ba81b137f8fc01ac5a31fb5", method: URLSessionManager.HTTPMethod.get) { (response: PosterModel) in
                        print(response.results!)
                        self.array_Poster = response.results ?? [PosterResult]()
                        group.leave()
                    } failure: { (error) in
                        print(error)
                    }

                    group.enter()
                    URLSessionManager.hitApi(urlString: "https://api.themoviedb.org/3/movie/popular?api_key=55957fcf3ba81b137f8fc01ac5a31fb5&language=en-US&page=\(pageNo)", method: URLSessionManager.HTTPMethod.get) { (response: PopularModel) in
                        self.array_Popular = response.results ?? [PopularResult]()
                        self.pageNo = self.pageNo+1
                        self.isPaginating = false
                        group.leave()
                    } failure: { (error) in
                        print(error)
                    }
                    
            group.notify(queue: DispatchQueue.main) {
                completion()
            }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : self.array_Popular.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            let cell:PosterTableViewCell = tableView.dequeueReusableCell(withIdentifier: "posterCell",
                                                               for: indexPath) as! PosterTableViewCell
            cell.configure(arra_poster: self.array_Poster)
            cell.clouser = { selectedObject in
                
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let dvc = storyboard.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
                dvc.movieID = selectedObject.id ?? 0
                dvc.modalTransitionStyle = .crossDissolve
//                dvc.modalPresentationStyle = .fullScreen
                self.present(dvc, animated: true) {
                    
                }
            }
            return cell
        }
        else
        {
            let cell:MovieCell = tableView.dequeueReusableCell(withIdentifier: "MovieCell",
                                                               for: indexPath) as! MovieCell
            cell.configure(obj_Popular: array_Popular[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let rect = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 35)
                let footerView = UIView(frame:rect)
                footerView.backgroundColor = UIColor.darkGray
        let title = UILabel.init(frame: CGRect(x: 15, y: 0, width: tableView.frame.size.width-15, height: 35))
        title.textColor = .systemYellow
        title.text = section == 0 ? "Playing Now" : "Most Popular"
        footerView.addSubview(title)
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 160 : 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35.0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
            if scrollView.contentOffset.y > self.moviesTableView.contentSize.height-(UIScreen.main.bounds.size.height+100) && isPaginating == false {
                    self.getMoviesData(withPage: self.pageNo)
            }
        }
    
    func stopAnimating(){
    self.moviesTableView.tableFooterView = .init()
    }

    func getMoviesData(withPage pageNo: Int){
        self.moviesTableView.tableFooterView = createSpinner
        isPaginating = true
        URLSessionManager.hitApi(urlString: "https://api.themoviedb.org/3/movie/popular?api_key=55957fcf3ba81b137f8fc01ac5a31fb5&language=en-US&page=\(pageNo)", method: URLSessionManager.HTTPMethod.get) { (response: PopularModel) in
            self.array_Popular.append(contentsOf: response.results ?? [PopularResult]())
            
            self.pageNo = self.pageNo+1
            self.isPaginating = false
            DispatchQueue.main.async {
                self.stopAnimating()
                self.moviesTableView.reloadData()
            }
        } failure: { (error) in
            print(error)
        }
    }
    
    
    
    
}
