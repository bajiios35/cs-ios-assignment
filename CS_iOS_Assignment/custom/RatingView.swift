//
//  RatingView.swift
//  CS_iOS_Assignment
//
//  Copyright © 2019 Backbase. All rights reserved.
//

import UIKit

class RatingView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initSubviews()
    }
    
    func initSubviews(value: Double = 0.0) {

        self.layoutIfNeeded()

        let centerPoint = CGPoint (x: self.bounds.width / 2, y: self.bounds.width / 2)
        let circleRadius : CGFloat = self.bounds.width / 2 * 0.83

        let circlePath = UIBezierPath(arcCenter: centerPoint, radius: circleRadius, startAngle: CGFloat(-0.5 * .pi), endAngle: CGFloat(1.5 * .pi), clockwise: true    )

        let progressCircle = CAShapeLayer()
        progressCircle.path = circlePath.cgPath
        if CGFloat(value/10.0) > 0.5
        {
            progressCircle.strokeColor = UIColor.green.cgColor
        }
        else
        {
            progressCircle.strokeColor = UIColor.systemYellow.cgColor
        }
        
        progressCircle.fillColor = UIColor.clear.cgColor
        progressCircle.lineWidth = 1.5
        progressCircle.strokeStart = 0
        progressCircle.strokeEnd = CGFloat(value/10.0)
        
        let progressCircle1 = CAShapeLayer()
        progressCircle1.path = circlePath.cgPath
        progressCircle1.strokeColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        progressCircle1.fillColor = UIColor.clear.cgColor
        progressCircle1.lineWidth = 1.5
        progressCircle1.strokeStart = 0
        progressCircle1.strokeEnd = 1.0

        self.layer.addSublayer(progressCircle1)
        self.layer.addSublayer(progressCircle)
        
    }
}
